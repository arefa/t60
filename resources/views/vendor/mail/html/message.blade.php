<?php
use Illuminate\Support\Facades\DB;

$site_info = DB::table('site_info')->get();
$info_element_array = array();
foreach ($site_info as $info_element) {
    $info_element_array[$info_element->attr_name] = $info_element->attr_value;
}
?>
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ $info_element_array['site_name']. ' Team' }}
                <img style="border-radius: 50%" height="50" width="50" src="{{ asset('uploads/avatars/'.$info_element_array['logo_pic']) }}" />
        @endcomponent
    @endslot
    {{-- Body --}}
    {{ $slot }}
    
    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
