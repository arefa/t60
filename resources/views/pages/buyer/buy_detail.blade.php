@extends('layouts.app')

@php
    $leftAds = \App\library\SiteHelper::getLeftAds('otherpages',0);
    $rightAds = \App\library\SiteHelper::getRightAds('otherpages',0);
    $allAds = array_merge($rightAds->toArray(),$leftAds->toArray());
    $allAdsCount = count($allAds);
    $addCnt = 0;

    $tot_posts = count($buyers) + count($sellers) + count($articles) + count($posts);
    $totLedtAdds = count($leftAds);
    $totrightAds = count($rightAds);
    $post_col = 'col-md-3';
    $repPrRow = ($totLedtAdds > 0) ? round( 6 / $totLedtAdds) : 0;
    $repPrRowR = $totrightAds > 0 ? ceil( 4 / $totrightAds) :0 ;
    $rows = ceil($tot_posts / 4 );
    if($setting->view_style == 'facebook'){
        $post_col = ' col-md-12 col-lg-12';
        $repPrRow = ($totLedtAdds > 0) ? ceil( 5 / $totLedtAdds) : 0;
        $repPrRowR = $totrightAds > 0 ? ceil( 4 / $totrightAds) : 0;
        $rows = ceil($tot_posts / 1 );
    }
@endphp
@section('content')
    <!--
<div class="<?php echo $setting->view_style == 'facebook' ? 'home-facebook' : 'home-pinterest' ?>"> -->
    <div>

        <div class="container">
            <br/>
            <div class="row text-dark" style="background-color: #f8f9fa;">
                <div class="col-auto col-md-3">
                    <ol class="breadcrumb"
                        style="margin: 4px;width: 100%;background-color: #e18904;margin-right: 0px;">
                        <li class="breadcrumb-item"><a href="#"><span
                                        style="color: rgb(249,249,249);">Home</span></a></li>
                        <li class="breadcrumb-item"><a href="#"><span
                                        style="color: rgb(255,255,255);">Library</span></a>
                        </li>
                        <li class="breadcrumb-item" style="background-color: #e18904;"><a href="#"><span
                                        style="color: rgb(255,255,255);">Data</span></a></li>
                    </ol>
                </div>
                <div class="col d-inline"
                     style="margin: 4px;width: 250px;background-color: rgba(156,156,156,0);margin-right: 0px;">
                    <h5 class="text-left" style="font-size: 20px;margin: 0px 0px 0px 5px;margin-top: 10px;">Green
                        Eyed
                        Cats</h5>
                </div>
                <div class="col col-md-4">
                    <button class="btn btn-primary btn-sm" type="button"><i
                                class="fa fa-check" style="margin: 4px;"></i>Edit
                    </button>
                    <button class="btn btn-primary btn-sm" type="button"
                            style="background-color: rgb(255,255,255);color: rgb(3,0,0);"><i class="fa fa-times"
                                                                                             style="margin: 5px;"></i>Cancel
                    </button>
                    <button
                            class="btn btn-primary btn-sm" type="button" style="background-color: rgb(225,137,4);">
                        <i
                                class="fa fa-trash" style="margin: 5px;"></i>Delete
                    </button>
                </div>
            </div>
        </div>
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-left">
                        <div class="alert alert-warning" role="alert" style="margin-top: 10px;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <span><strong>You'll need to sign in or register before bidding.</strong></span></div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col col-md-12" style="width: 100%;"><img class="img-fluid"
                                                                                 src="{{asset('images/maruti-suzuki-baleno.jpg')}}"
                                                                                 width="100%">
                            </div>
                            <div class="col-auto col-md-12">
                                <h4 style="margin-top: 5px;">Heading</h4>
                                <p class="lead text-break" style="width: 100%;">A&nbsp;<strong>paragraph</strong>&nbsp;is
                                    a
                                    series of sentences that are organized and coherent, and are all related to a
                                    single
                                    topic. Almost every piece of writing you do that is longer than a few sentences
                                    should
                                    be organized
                                    into&nbsp;<strong>paragraphs</strong>.<br><br></p>
                            </div>
                            <div class="col-2 float-left col-md-2"><i class="fa fa-thumbs-o-up"
                                                                      style="font-size: 15px;margin-left: 2px;"></i><span
                                        style="font-size: 15px;">Like</span><label class="text-center"
                                                                                   style="width: 100%;">(0)</label>
                            </div>
                            <div class="col-2 col-md-2"><i class="fa fa-thumbs-o-down"
                                                           style="font-size: 15px;"></i><span
                                        style="font-size: 15px;">DisLike</span><label class="text-center"
                                                                                      style="width: 100%;">(0)</label>
                            </div>
                            <div class="col-2 col-md-2"><i
                                        class="fa fa-heart d-sm-flex d-lg-flex justify-content-sm-center justify-content-lg-center"
                                        style="font-size: 20px;margin-left: 0;width: 100%;"></i><label
                                        class="text-center"
                                        style="width: 100%;">(0)</label>
                            </div>
                            <div class="col-2 col-md-2"><i
                                        class="fa fa-frown-o d-sm-flex d-lg-flex justify-content-sm-center justify-content-lg-center"
                                        style="font-size: 20px;margin-left: 0;width: 100%;"></i><label
                                        class="text-center"
                                        style="width: 100%;">(0)</label>
                            </div>
                            <div class="col-2 offset-0 col-md-2"><i
                                        class="fa fa-meh-o d-sm-flex d-lg-flex justify-content-sm-center justify-content-lg-center"
                                        style="font-size: 20px;margin-left: 0;width: 100%;"></i><label
                                        class="text-center"
                                        style="width: 100%;">(0)</label>
                            </div>
                            <div class="col-2 d-inline col-md-2"><i
                                        class="fa fa-meh-o d-sm-flex d-lg-flex justify-content-sm-center justify-content-lg-center"
                                        style="font-size: 20px;margin-left: 0;width: 100%;"></i><label
                                        class="text-center"
                                        style="width: 100%;">(0)</label>
                            </div>
                            <div class="col col-md-12"
                                 style="margin-top: 20px;"><img class="img-fluid"
                                                                src="{{asset('uploads/avatars/1579236544.png')}}"
                                                                width="100px" height="100px"
                                                                style="padding: 5px;width: 50px;height: 50px;"><label
                                        class="text-left align-items-center align-self-start flex-wrap m-auto"
                                        style="font-size: 20px;padding: 5px;margin-bottom: 0px;">Arefa</label>
                                <label
                                        class="text-left align-items-center align-self-start flex-wrap m-auto"
                                        style="font-size: 20px;padding: 5px;margin-bottom: 0px;"><i
                                            class="fa fa-calendar"
                                            style="margin-right: 5px;"></i>26-JAN-2020</label><label
                                        class="text-left align-items-center align-self-start flex-wrap m-auto"
                                        style="font-size: 20px;padding: 5px;margin-bottom: 0px;color: rgb(254,25,10);"><i
                                            class="fa fa-trash-o" style="margin-right: 5px;"></i>Delete</label>
                                <p>A&nbsp;<strong>paragraph</strong>&nbsp;is a series of sentences that are
                                    organized and
                                    coherent, and are all related to a single topic. Almost every piece of writing
                                    you do
                                    that is longer than a few sentences should be
                                    organized into&nbsp;<strong>paragraphs</strong>.<br><br><br></p><label
                                        style="font-size: 20px;font-weight: bold;font-style: normal;">Leave a
                                    Comment</label><textarea class="form-control-lg" style="width: 100%;" rows="30"
                                                             cols="30" wrap="soft"></textarea>
                                <div class="btn-group" role="group" style="margin-top: 10px;">
                                    <button class="btn btn-primary" type="button">POST COMMENT</button>
                                    <button class="btn btn-danger" type="button" style="margin-left: 2px;">DELETE
                                        POST
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card bg-light" style="margin-top: 10px;">
                            <div class="card-body text-left">
                                <h4 class="card-title">Highest bid $60.00</h4>
                                <hr>
                                <h6 class="text-muted card-subtitle mb-2"></h6>
                                <p class="card-text">Bid Dedline September 8,2020 and close in 7 month from now</p>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="input-group">

                                            <input class="form-control" type="text">
                                            <div class="input-group-prepend"><span class="input-group-text">00</span>
                                            </div>

                                        </div>
                                        <button class="btn  btn-primary" type="button">Place Bid</button>

                                    </div>

                                    <div class="col-6">
                                        <label style="color: rgb(221,144,28);font-size: 15px;">Bid
                                            History</label><label class="d-block d-lg-flex align-items-lg-start">Total
                                            Bid
                                            &nbsp;
                                            &nbsp; &nbsp;: 2</label><label class="d-lg-flex align-items-lg-start">Total
                                            Order &nbsp;:
                                            2</label>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="card bg-light" style="margin-top: 10px;">
                            <div class="card-body text-left">
                                <h4 class="card-title">Genral Info</h4>
                                <hr>
                                <h6 class="text-muted card-subtitle mb-2"></h6>
                                <div class="input-group">
                                    <div class="input-group-prepend"></div>
                                    <div class="input-group-append"></div>
                                </div>
                                <ul class="list-group bg-light">
                                    <li class="list-group-item bg-light border-light" style="padding: 0px;margin: 0px"><i
                                                class="fa fa-money"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;Price &nbsp; &nbsp; &nbsp;
                                            &nbsp;  &nbsp;</label><label
                                                style="font-size: 15px;">$50.00</label></li>
                                    <li class="list-group-item bg-light border-light" style="padding: 0px;"><i
                                                class="fa fa-copyright"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;Country &nbsp;
                                            &nbsp;&nbsp;&nbsp;</label><label
                                                style="font-size: 15px;">India</label></li>
                                    <li class="list-group-item bg-light border-light" style="padding: 0px;"><i
                                                class="fa fa-money"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;State &nbsp; &nbsp; &nbsp;
                                            &nbsp;&nbsp;</label><label
                                                style="font-size: 15px;">Bulgariya</label></li>
                                    <li class="list-group-item bg-light border-light" style="padding: 0px;"><i
                                                class="fa fa-money"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;CIty &nbsp; &nbsp; &nbsp; &nbsp;
                                            &nbsp;&nbsp;</label><label
                                                style="font-size: 15px;">Bangaluru</label></li>
                                    <li
                                            class="list-group-item bg-light border-light" style="padding: 0px;"><i
                                                class="fa fa-money"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;Address &nbsp;  </label><label
                                                style="font-size: 15px;">&nbsp;&nbsp;ny,303 avenueroad</label></li>
                                    <li class="list-group-item bg-light border-light" style="padding: 0px;"><i
                                                class="fa fa-money"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;Posted at &nbsp;</label><label
                                                style="font-size: 15px;">January 7,2018</label>
                                    </li>
                                    <li class="list-group-item bg-light border-light" style="padding: 0px;"><i
                                                class="fa fa-money"
                                                style="font-size: 15px;"></i><label
                                                style="font-size: 15px;">&nbsp;Expired at &nbsp;</label><label
                                                style="font-size: 15px;">September 9,2020</label>
                                    </li>
                                </ul>
                                <br/>
                                <h6 style="font-weight: bold;">Share This Ad</h6><i class="fa fa-facebook-square"
                                                                                    style="font-size: 20px;"></i>
                            </div>
                        </div>
                        <div class="card bg-light" style="margin-top: 10px;">
                            <div class="card-body text-left" style="margin-top: 10px;">
                                <div><img class="float-left" src="{{asset('uploads/avatars/1579236544.png')}}"
                                          width="50px" height="50px">
                                    <h6 class="text-center float-left" style="margin-top:0px;margin-left: 2px;">John
                                        doe</h6>
                                </div>
                                <div class="float-right" style="margin-left: 2px;">
                                    <p class="lead text-break float-left d-xl-flex align-items-xl-center"
                                       style="margin-left: 5px;">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <br></p>
                                    <br/>
                                    <span><i class="fas fa-rss"
                                             style="font-size: 20px;"></i></i>&nbsp;&nbsp;Facebook</span>

                                </div>
                                <div class="float-left" style="width: 100%;margin-top: 10px;">
                                    <ul class="list-unstyled">
                                        <li style="font-size: 15px;color: rgb(245,134,4);"><i
                                                    class="fa fa-user"></i>&nbsp;View
                                            All ads of this Seller
                                        </li>
                                        <li style="font-size: 15px;color: rgb(245,134,4);"><i
                                                    class="fa fa-star-o"></i>&nbsp;Save
                                            ad as favorite
                                        </li>
                                        <li style="font-size: 15px;color: rgb(245,134,4);"><i class="fa fa-ban"></i>&nbsp;Report
                                            this ad
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card bg-light" style="margin-top: 10px;">
                            <div class="card-body text-left" >
                                <div>
                                    <h5 class="text-center float-left" style="margin-left: 10px;">Similar
                                        Auctions</h5>
                                </div>
                                <div class="float-left" style="width: 100%;margin-top: 10px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="d-xl-flex justify-content-xl-start">Bid History</h1>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Bidder</th>
                                    <th>Bid Amount</th>
                                    <th>Date Time</th>
                                    <th>Buyer</th>
                                    <th>Seller</th>
                                    <th>Status</th>
                                    <th rowspan="2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>

                                        <img class="float-left" src="{{asset('uploads/avatars/1579236544.png')}}"
                                             width="30px" height="30px">
                                        Bidder #1

                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star" style="font-size:10px "></span>
                                        <span class="fa fa-star" style="font-size:10px "></span>

                                    </td>
                                    <td>$16.00</td>
                                    <td>January 7,2018 4:25 PM</td>
                                    <td>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star" style="font-size:10px "></span >
                                        <span class="fa fa-star" style="font-size:10px "></span>
                                    </td>
                                    <td>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star checked" style="font-size:10px "></span>
                                        <span class="fa fa-star" style="font-size:10px "></span>
                                        <span class="fa fa-star" style="font-size:10px "></span>
                                    </td>
                                    <td>Pandding</td>
                                    <td colspan="2">
                                        <div class="btn-group" role="group">
                                            <button class="btn btn-primary" type="button">Order</button>
                                            <button class="btn btn-primary" type="button" style="margin-left: 2px;">
                                                Dispatch
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            {{--<div class="col-md-3">--}}
            {{--<div class="card text-center card-width">--}}
            {{--<div class="card-header">--}}
            {{--<div class="row no-margin" style="width: 100%">--}}
            {{--<div class="col-md-2" style="padding-right: 0; padding-left: 2px">--}}
            {{--<div class="img-container">--}}
            {{--<img width="100%" src="http://hi5working.test/uploads/avatars/1542355689.jpg">--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-5" style="padding-left: 0;text-align: left">--}}
            {{--<div class="title-container ml-2"><span style="position: absolute;bottom: 0;">Elu</span></div>--}}
            {{--</div>--}}
            {{--<div class="col-md-5 align-self-end">--}}
            {{--<input id="ownRatingMobileCard" name="ownRating" class="rating rating-loading own-rating rating-xs ownRatingMobileCard"--}}
            {{--value="{{averageReview(Auth::user()->id)}}" style="padding-top: 8px;">--}}

            {{--</div>--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--<div class="card-body">--}}
            {{--<div class="row">--}}
            {{--<div class="col">--}}
            {{--<img width="100%" src="http://hi5working.test/uploads/avatars/1542355689.jpg">--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<h5 class="card-title">Elu@Elu.com</h5>--}}
            {{--</div>--}}
            {{--<div class="card-footer text-muted">--}}
            {{--<a href="#" class="btn btn-primary">Go somewhere</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}


        </div>
    </div>
@endsection
@section('extra-JS')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".share-save-icon-buyr").click(function () {
                var user_id = $(this).attr("data-value");
                var post_id = $(this).attr("id");
                var post_type = $(this).attr("data-value1");
                var saved_cls = $(this).attr("data-value-2");
                var status = "";
                if (saved_cls == "blue") {
                    $(this).addClass('hide');
                    $(this).next().removeClass('hide');
                    // $(".blue").addClass('hide');
                    // $(".yellow").removeClass('hide');
                    status = 1;
                } else {
                    $(this).addClass('hide');
                    $(this).prev().removeClass('hide');
                    // $(".yellow").addClass('hide');
                    // $(".blue").removeClass('hide');
                    status = 0;
                }
                $.ajax({
                    url: "SavePost",
                    type: "POST",
                    data: {
                        user_id: user_id,
                        post_id: post_id,
                        post_type: post_type,
                        status: status
                    },
                    dataType: "JSON",
                    success: function (data) {
                        console.log(data);

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert('Something went wrong');
                    }
                });
            });
        });

        function showMsg(heading, amount, owner_id, id) {
            Swal.fire({
                title: 'Pay To Read?',
                text: "You have to pay $" + amount + " to read!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'green',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Pay'
            }).then((result) = > {
                if (result.value
        )
            {
                $.ajax({

                    url: '/blog/pay-to-read/' + amount + '/' + owner_id + '/' + id,
                    type: 'GET',

                    success: function (response) {

                        console.log(response);
                        Swal.fire(
                            'Payment Done!',
                            'You can now read blog.',
                            'success'
                        ).then((result) = > {
                            if (result.value
                        )
                        {
                            window.location = '/blod-details/' + id;
                        }
                    })
                        ;

                    }
                });
            }
        })
            ;
        }
    </script>
    <div id="fb-root"></div>
    <script>
        //shere event

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=403257377055066";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function fb_share(dynamic_link, dynamic_title) {
            var app_id = '403257377055066';
            var pageURL = "https://www.facebook.com/dialog/feed?app_id=" + app_id + "&link=" + dynamic_link;
            var w = 600;
            var h = 400;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open(pageURL, dynamic_title,
                'toolbar=no, location=no, directories=no,status=no, menubar=yes, scrollbars=no, resizable=no, copyhistory=no, width=' +
                800 + ',height=' + 650 + ', top=' + top + ', left=' + left)

            return false;
        }
    </script>
@endsection