<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@if ($setting = \App\Setting::first())
    <meta name="pusher_app_key" content='{{ $setting->pusher_app_key }}'>
    <meta name="pusher_app_cluster" content='{{ $setting->pusher_app_cluster }}'>
    <meta property="og:image:width" content="1381">
    <meta property="og:image:height" content="723">
    <meta property="og:description" content="Description here">
    <meta property="og:url" content="http://splashthemepark.com/home">
    <meta property="og:image" content="http://splashthemepark.com/home/uploads/avatars/og-image.jpg">
    <meta property="og:title" content="title here">
    <meta property="og:referalcode" content="referalcode of user here">
@endif
<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
<style>
    html{
        margin: 0px !important;
    }
    .main-cont{
        /* margin: 50px; */
    }
    .cls-left-add-title, .cls-right-add-title{
    transform: translate(0px, 0px);
}
    .cls-left-add-title span,
    .exampleSlider .MS-content span {
        height: 22px;
        background-color: #000;
        width: 100%;
        display: block;
        line-height: 22px;
        text-align: center;
        bottom: 0px;
        position: absolute;
        opacity: 0.8;
    }

    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;

        color: white;
        text-align: center;
    }

    .cls-social {
        background-color: orange;
    }

    .adsMargin {
        background-color: #fff;
    }

    @media (max-width: 700px) {
        .foot-add-row {
            display: none;
        }
    }

    .heart {
        cursor: pointer;
        height: 50px;
        width: 50px;
        background-image: url('https://abs.twimg.com/a/1446542199/img/t1/web_heart_animation.png');
        background-position: left;
        background-repeat: no-repeat;
        background-size: 2900%;
    }

    .heart:hover {
        background-position: right;
    }

    .is_animating {
        animation: heart-burst .8s steps(28) 1;
    }

    @keyframes heart-burst {
        from {
            background-position: left;
        }

        to {
            background-position: right;
        }
    }


    .text-white {}

    .look-like-btn {
        background-color: Transparent;
        background-repeat: no-repeat;
        border: none !important;
        cursor: pointer !important;
        overflow: hidden;
        color: #364759;
        box-shadow: none !important;
    }

    .cls-right-add-img img {
        width: 100%;
        min-height: 100px;
    }

    .cls-right-add-block ,.cls-left-add-block{
        margin-bottom: 10px;
    }

    .cls-right-add-block div {
        padding: 0px;
        padding-left: 2px;
    }

    .cls-right-add-img span {
        position: absolute;
        transform: translate(-100%, 0px);
        background: #000;
        color: #fff;
        opacity: 0.6;
        width: 100%;
        bottom: 10px;
        height: 30px;
        text-align: center;
    }


    .sm-add-root {
        margin-bottom: 10px;
        max-height: 200px;
    }

    .sm-add-root img {
        width: 100%;
        min-height: 100px;
        max-height: 200px;
    }

    .sm-add-root div {
        padding: 0px;
        padding-left: 2px;
    }

    .sm-add-root span {
        position: absolute;
        transform: translate(-100%, 170px);
        background: #000;
        color: #fff;
        opacity: 0.6;
        width: 93%;
        height: 30px;
        text-align: center;
    }

    .cls-left-add-root.d-none.d-md-block {
        padding-left: 14px;
    }

    .foot-add-row .col-md-1 span {
        position: absolute;
        background: black;
        transform: translate(-100%, 0%);
        width: 101%;
        bottom: 0;
        height: 38px;
        opacity: 0.5;
    }

    .foot-add-row .col-md-1 {
        margin: 0px;
        padding: 0px;
    }

    .foot-add-row .col-md-1 img {
        width: 100%;
        height: 100px;
    }

    .overlay_badge_buy.buyDelete,
    .overlay_badge_buy.eventDelete,
    .overlay_badge_buy.buyEdit,
    .overlay_badge_buy.eventEdit,
    .overlay_badge_buy.savedButton {
        max-width: 30px;
        float: right;
        left: unset !important;
        right: 0px;
    }

    .overlay_badge_buy.buyDelete,
    .overlay_badge_buy.eventDelete {
        right: 65px
    }

    .overlay_badge_buy.buyEdit,
    .overlay_badge_buy.eventEdit {
        right: 32px
    }

    .cls-add-row {
        /* min-height:<?php echo $setting->view_style == 'facebook' ? 400 : 450 ?>px; */
    }

    .cls-add-row img,
    .cls-add-row div {
        max-width: 100%;
        min-width: 100px;
        width:100%;
    }

    .root-home-pinterest .col-lg-1.col-md-1 .cls-right-add-img span {
        transform: translate(0px, 10px);
    }

    @media(min-width: 1200px) {

        .cls-cnt {
            min-width: <?php echo $setting->view_style=='facebook'? 50: 80 ?>% !important;
            max-width: <?php echo $setting->view_style=='facebook'? 50: 80 ?>% !important;
        }

        .cls-left-add-root,
        .cls-right-add-root {
            min-width: <?php echo $setting->view_style=='facebook'? 25: 9 ?>% !important;
            padding: 20px;
        }

    }

    /* Left Right Add Start */

    .cls-left-add-root,
    .cls-right-add-root {
        float: left;
        max-width: 100px;
    }

    /* Left Right Add end */
</style>
@include('partials.head')
<?php
use Illuminate\Support\Facades\DB;
$site_info = DB::table('site_info')->get();
$info_element_array = array();
foreach ($site_info as $info_element) {
    $info_element_array[$info_element->attr_name] = $info_element->attr_value;
}
?>
{{--<body style="background: url('/uploads/avatars/{{$info_element_array['above_footer_pic']}}'); background-size:
cover;" >--}}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>
@include('partials.ModelPopup')
@include('partials.second_nav')
@include('partials.nav')

@php
$currentPage = 'otherpages';
$adsUserId = 0;
@endphp
@if ($controller == 'UserController')
@php
$currentPage = 'profile';
$adsUserId = $user->id;
@endphp
@endif
@php

$popupAds = \App\library\SiteHelper::getPopupAds($currentPage,$adsUserId);
@endphp
@if($popupAds->count() > 0)
<div id="open-modal" class="modal-window modal-sm" style="display: block;padding:0px;">
    <div>
        <div class="closebtnarea" align="right"><i title="Close" onclick="$('#open-modal').hide('slow');"
                class="fa fa-times-circle fl-r crs-pntr" style="font-size:27px;color:#6c757d;"></i></div>
        <br>
        <div class="container">
            <div class="">
                @foreach($popupAds as $popupAd)
                <div class="col-md-12 margin-bottom-15"
                    style="padding-right: 10px;padding-bottom: 10px;padding-left: 10px;">
                    <div class="adsContent">
                        @if($popupAd->adds_type == 'image')
                        <a class="adsLink" href="{{ 'http://'.$popupAd->image_link }}" target="_blank">
                            <img src="{{ asset('/uploads/adsImages/'.$popupAd->image) }}" class="adsImage"
                                style="height:300px;border-radius: 10px;">
                        </a>
                        @elseif($popupAd->adds_type == 'embed_code')
                        <?php echo $popupAd->embed_code ?>
                        @elseif($popupAd->adds_type == 'referral_code')
                        {{ $popupAd->referral_code }}
                        @else
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div style="position:fixed; right:0%; bottom:0%;z-index: 2;">
    <div id="messagepop"></div>
</div>

@endif
@php
$leftAds = \App\library\SiteHelper::getLeftAds($currentPage ,$adsUserId);
$rightAds = \App\library\SiteHelper::getRightAds($currentPage ,$adsUserId);
$addcls = 'col-lg-2 col-md-2';
$contCls =' col-lg-6 col-md-6';
if (isset($homeList)) {
if ($setting->view_style !== 'facebook') {
$addcls = 'col-lg-2 col-md-1';
$contCls =' col-lg-8 col-md-10';
}
} else {
$addcls = 'col-lg-2 col-md-2';
$contCls =' col-lg-8 col-md-8';
}

@endphp
{{-- left add start --}}
<div class="">

    <div
        class="main-cont row <?php echo (isset($homeList) ? 'cont-home-page' : 'cont-other-page')  ?> <?php echo $setting->view_style == 'facebook' ? 'root-home-facebook' : 'root-home-pinterest' ?>">
        @if (isset($homeList) && $setting->view_style == 'facebook' )

        <div class="col-lg-1 col-md-1 d-none d-md-block"></div>
        @endif

        <div class="cls-left-root d-none d-md-block {{$addcls}}" id="left-add-root">
            <div class="cls-add-row sticky-top ">
                @foreach ($leftAds as $value)
                <div class="cls-left-add-block">
                    <a href="{{ ($value->image_link ) ? $value->image_link : '#' }}"
                        {{ ($value->image_link) ? 'target="_blank"' : '' }} class="add-link">
                        <div class="cls-left-add-img">
                            <img class=""
                                src="{{$value->image? asset("uploads/adsimages/".$value->image):asset("images/image_not_found.jpg")}}"
                                width="200px" alt="Card image cap">
                        </div>
                        <div class="cls-left-add-title">
                            <span class="">{{$value->adds_name}} </span>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
        <div class="cls-containt-rooot {{$contCls}} col-sm-12 ">

            {{-- left add ennd --}}
            @yield('content')
            {{--  --}}
            
@if (!isset($homeList))
<?php 

$botAdsot = \App\library\SiteHelper::getBottomAds($currentPage ,$adsUserId)->toArray();
$topAdsot = \App\library\SiteHelper::getTopAds($currentPage ,$adsUserId)->toArray();
// $allAds = array_merge($rightAds->toArray(),$leftAds->toArray(), $topAdsot, $botAdsot);
$allAds = array_merge( $botAdsot);
$allAdsCount = count($allAds);
$addCnt = 0;
    ?>
    @if ($allAdsCount > 0 && $addCnt < $allAdsCount) <br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row justify-content-center "> <?php
                for ($addCnt=0; $addCnt < $allAdsCount; $addCnt++) { ?>
                    <div class="col-md-12 cls-bottom-add-item ">
                        <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                            {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                            <img class="" width="100%"
                                src="{{$allAds[$addCnt]['image']?asset("uploads/adsimages/".$allAds[$addCnt]['image']):asset("images/image_not_found.jpg")}}"
                                alt="Card image cap">
                            <br>
                            <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endif
            {{--  --}}
        </div>
        <div class="cls-right-add-root-new d-none d-md-block   d-none d-md-block {{$addcls}}" id="right-add-root">
            <?php
            ?>
            <div class="cls-add-row sticky-top">
                @foreach ($rightAds as $value)

                <div class="cls-right-add-block">
                    <a href="{{ ($value->image_link ) ? $value->image_link : '#' }}"
                        {{ ($value->image_link) ? 'target="_blank"' : '' }} class="add-link">
                    <div class="cls-right-add-img">
                            <img class=""
                                src="{{$value->image? asset("uploads/adsimages/".$value->image):asset("/images/image_not_found.jpg")}}"
                                alt="Card image cap">

                            </div>
                             <div class="cls-left-add-title">
                            <span class="">{{$value->adds_name}} </span>
                        </div>
                            
                    </a>
                </div>
                @endforeach

            </div>

        </div>
        @if (isset($homeList) && $setting->view_style == 'facebook' )

        <div class="col-lg-1 col-md-1 d-none d-md-block"></div>
        @endif

    </div>

</div>
{{--  --}}

@if (!isset($homeList))
<?php 

$botAdsot = \App\library\SiteHelper::getBottomAds($currentPage ,$adsUserId)->toArray();
$topAdsot = \App\library\SiteHelper::getTopAds($currentPage ,$adsUserId)->toArray();
// $allAds = array_merge($rightAds->toArray(),$leftAds->toArray(), $topAdsot, $botAdsot);
$allAds = array_merge( $botAdsot);
$allAdsCount = count($allAds);
$addCnt = 0;
    ?>
    @if ($controller !== 'UserController')
    @php
    $leftAds = \App\library\SiteHelper::getLeftAds('otherpages',0);
    $rightAds = \App\library\SiteHelper::getRightAds('otherpages',0);
    $allAds = array_merge($rightAds->toArray(),$leftAds->toArray());
    $addCnt = 0;
    $allAdsCount = count($allAds);
    @endphp
    <?php while ($addCnt < $allAdsCount) { ?>
    <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
    <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
        <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
            {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
            <img class=""
                src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                alt="Card image cap">
            <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
        </a>
        <?php 
            $addCnt++; 
            ?>
    </div>
    <?php } } ?>
    @endif
    @endif
    <style>
        /* Left  and right add start *
        .cls-left-root, .cls-containt-rooot{
            float: left;
        }
        .main-cont.cont-home-page.root-home-facebook .cls-add-row {
            max-width: 114px;
        }

        .main-cont.cont-home-page.root-home-facebook .cls-add-row .cls-left-add-img img {
            max-width: 100%;
        }

        .main-cont.cont-home-page.root-home-facebook .container {
            max-width: 650px;
        }
        /* Left  and right add end*/

        .root-home-facebook .cls-containt-rooot.col-lg-6.col-md-6 {
            padding: 0px;
            margin: 0px;
        }

        .cls-footer {
            z-index: 99999;

        }

        .cls-footer .MS-content,
        .cls-footer .adsMargin {
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }

        .cls-bottom-add-item img {
            max-height: 400px
        }

        .cls-bottom-add-item a span {
            opacity: 0.8;
            width: 100%;
            height: 25px;
            line-height: 25px;
            display: block;
            background-color: #000;
            text-align: center;
            position: relative;
            bottom: 25px;
        }

        ul.footer-social li {
            margin: 27px 5px 0 5px;
            padding: 0;
        }

        .social-section {
            width: 100%;
            height: 45px;
            text-align: center;
        }

        ul.footer-social li {
            height: 30px;
            font-size: 20px;
            display: inline-block;
            margin: 10px 11px 0 11px;
        }

        ul.footer-social li a {
            color: #FFF;
        }
    </style>
    <!-- Scroll top buttm -->
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <script>
        mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>
    <style>
        #myBtn {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Fixed/sticky position */
            bottom: 20px;
            /* Place the button at the bottom of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
            border: none;
            /* Remove borders */
            outline: none;
            /* Remove outline */
            background-color: orange;
            /* Set a background color */
            color: white;
            /* Text color */
            cursor: pointer;
            /* Add a mouse pointer on hover */
            padding: 15px;
            /* Some padding */
            border-radius: 10px;
            /* Rounded corners */
            font-size: 18px;
            /* Increase font size */
        }

        #myBtn:hover {
            background-color: #c38108;
            /* Add a dark-grey background on hover */
        }
    </style>
    <!-- Scroll top buttm -->

    <div class="footer d-none  cls-footer">
        <div class="row  cls-social">
            <div class="col-md-12 social-section">
                <ul class="footer-social">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
        </div>
        @if(isset($controller) && in_array( $controller, ['HomeController', 'PublicPageController']))
        <div class=" adsMargin">
            <div class="">
                <div class="exampleSlider">
                    <div class="MS-content">
                        @php
                        $bottomAds = \App\library\SiteHelper::getBottomAds($currentPage,$adsUserId);
                        $countBottomAdds = (sizeof($bottomAds));
                        @endphp
                        @foreach($bottomAds as $bottomAd)
                        <div class="item">
                            <a href="{{ 'http://'.$bottomAd->image_link }}"><img height="50px"
                                    style="max-height:100px;height:100px"
                                    src="{{ asset('/uploads/adsImages/'.$bottomAd->image) }}"
                                    class="img-responsive"></a>
                            <span>{{$bottomAd->adds_name}} </span>

                        </div>
                        @endforeach


                        <div class="MS-controls">
                            <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
                            <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    </div>

    <div style="position:fixed; right:0%; bottom:0%;z-index:2">
        <div id="messagepop">

        </div>
    </div>
    @include('partials.footer')
    @include('partials.filter-modal')
 
    @include('partials.JS')
    @include('partials.ModelPopupJs')

    {{--<div class="footer"--}}
    {{--style="background-image: url('/uploads/avatars/{{$info_element_array['footer_pic']}}'); min-height: 30px;
    background-size: cover;">--}}

    {{--</div>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <style>
        .fixed {
            position: fixed;
        }
    </style>
    <script>
        var idleTime = 0;
        $(document).ready(function () {
            // stiky();
            // stikyRight();
            //Increment the idle time counter every minute.
            var idleInterval = setInterval(timerIncrement, 1000); // 1 minute

            //Zero the idle timer on mouse movement.
            $(this).bind('mousewheel', function (e) {
                idleTime = 0;
                $('.cls-footer').addClass('d-none');
            });
            $(this).mousemove(function (e) {
                // idleTime = 0;
                // $('.cls-footer').addClass('d-none');
            });
            $(this).keypress(function (e) {
                $('.cls-footer').addClass('d-none');
                idleTime = 0;
            });
        });

        function timerIncrement() {
            idleTime = idleTime + 1;
            if (idleTime > 3) { // 
                $('.cls-footer').removeClass('d-none');
            }
        }
    </script>

    @stack('footerJs')
    </body>

</html>