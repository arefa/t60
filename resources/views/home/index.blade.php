@extends('layouts.app')


@php
$leftAds = \App\library\SiteHelper::getLeftAds('otherpages',0);
$rightAds = \App\library\SiteHelper::getRightAds('otherpages',0);
$allAds = array_merge($rightAds->toArray(),$leftAds->toArray());
$allAdsCount = count($allAds);
$addCnt = 0;

$tot_posts = count($buyers) + count($sellers) + count($articles) + count($posts);
$totLedtAdds = count($leftAds);
$totrightAds = count($rightAds);
$post_col = 'col-md-4';
$repPrRow = ($totLedtAdds > 0) ? round( 6 / $totLedtAdds) : 0;
$repPrRowR = $totrightAds > 0 ? ceil( 4 / $totrightAds) :0 ;
$rows = ceil($tot_posts / 4 );
if($setting->view_style == 'facebook'){
$post_col = ' col-md-12 col-lg-12';
$repPrRow = ($totLedtAdds > 0) ? ceil( 5 / $totLedtAdds) : 0;
$repPrRowR = $totrightAds > 0 ? ceil( 4 / $totrightAds) : 0;
$rows = ceil($tot_posts / 1 );
}
@endphp
@section('content')
<style type="text/css">
    .overlay_badge_sell {
        position: absolute;
        top: 0;
        left: 0;
        background-color: red;
        color: white;
        padding-left: 10px;
        padding-right: 10px;
    }

    .overlay_badge_buy {
        position: absolute;
        float: right;
        top: 0;
        left: 0;
        background-color: green;
        color: white;
        pointer-events: auto;
        padding-left: 10px;
        padding-right: 10px;
    }
    .overlay_badge_blog {
        position: absolute;
        float: right;
        top: 0;
        left: 0;
        background-color: #F4A460;
        color: white;
        pointer-events: auto;
        padding-left: 10px;
        padding-right: 10px;
    }
    .overlay_badge_event {
        position: absolute;
        float: right;
        top: 0;
        left: 0;
        background-color: purple;
        color: white;
        pointer-events: auto;
        padding-left: 10px;
        padding-right: 10px;
    }
    .divback {
        background-color: #e3e3e3;
        padding: 5px;
        margin: 5px;
    }

    .bigtext {
        color: black;
        font-weight: bolder;
    }

    .ordertext {
        color: #a658a6;
    }

    .bidtext {
        color: #4e66c4;
        align-self: flex-end;
    }

    .typing {
        background-color: #f47e2b;
        border-color: #f47e2b;
    }

    .typing i {
        display: block !important;
    }

    .cls-padding-2 {
        padding: 3px;
    }

    #timeText {
        color: black !important;
    }


    .cls-left-add-block {
        margin-bottom: 10px;
    }

    .cls-left-add-block div {
        padding: 0px;
        padding-left: 2px;
    }

    .footer {
        position: fixed;
    }

    .MYcontainer {

        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    .font-family-poppins {
        font-family: 'Poppins', sans-serif !important;
    }
</style>
<div class="container">
    <!--
<div class="<?php echo $setting->view_style == 'facebook' ? 'home-facebook' : 'home-pinterest' ?>"> -->
    <div>

        <div class="row">
            @isset($buyers)
            @foreach($buyers as $buyer)
            <?php
                            $checkPermission = DB::table('user_menu')->where('menu_options_id', '=', '18')->where('user_id', '=', auth()->id())->get()->first();
                            if ($checkPermission) {
                                $permission = 1;
                            } else {
                                $permission = 0;
                            }
                            ?>
            @php $bidAmount=bidAmountIfAlreadyBid($buyer,'buy');
            $isPostSaved=isSavedPost($buyer->id,'buy',auth()->id());
            @endphp
            @php $bid=$buyer->bids()->where('user_id',auth()->id())->first() @endphp
            <div class="{{$post_col}} col-sm-12 hoverSet">
                <div class="card shadow rounded mb-3 set">

                    <div class="position-relative">
                        <a href="#" data-bids="{{getPostTotalBids($buyer,'buy')}}"
                            data-orders="{{getPostBidOrders($buyer,'buy')}}" data-isSaved="{{$isPostSaved}}"
                            data-user-name="{{$buyer->user->name}}" data-avatar="{{$buyer->user->avatar}}"
                            data-all="{{json_encode($buyer)}}"
                            onclick="showPostDetails('{{$buyer->id}}','{{auth()->id()}}',this)">
                            <img class="card-img-top"
                                src="{{$buyer->buyer_featured_image?"uploads/buyer/".$buyer->buyer_featured_image:"/images/image_not_found.jpg"}}"
                                style="height: 200px" alt="Card image cap">
                        </a>
                        <span class="overlay_badge_buy">Buy ${{getCurrentRate($buyer)}}</span>
                        @if(auth()->id()==$buyer->user_id || $permission==1)
                        <span class="overlay_badge_buy buyDelete" id="buyDelete" style="background: none;padding: 0;">
                            <a href="#" onclick="deleteBuy('{{$buyer->id}}')">
                                <i class="fas fa-trash-alt text-dark bg-white p-1"></i>
                            </a>
                        </span>
                        <span class="overlay_badge_buy buyEdit" id="buyEdit" style="background: none;padding: 0;">
                            <a href="#" onclick="buyEdit('{{$buyer->id}}','{{$buyer->user_id}}')" class="">
                                <i class="fas fa-edit text-primary bg-white p-1"></i>
                            </a>
                        </span>
                        @endif
                        <span class="overlay_badge_buy savedButton" id="savedButton"
                            style="background: none;padding: 0;">
                            <a href="#" onclick="saveBuySell('{{$buyer->id}}','{{auth()->id()}}','buy')">
                                <i id="savedBuy{{$buyer->id}}" class="fas fa-heart text-danger bg-white p-1"></i>
                                
                            </a>
                        </span>
                        <div class="text-right mb-2 position-absolute" style="right:0; bottom: -6px;">
                            <span data-time="{{$buyer->created_at->addHours($buyer->hour)}}"
                                class="bg-danger text-white p-1 countDownTimer" id="showCountDownTimer">
                                {{$buyer->hour}}</span>
                        </div>
                    </div>
                    <div class="row mb-2 position-relative" style="font-size: 0.72rem !important;">
                        {{-- <div class="col-md-3 pl-4 pr-1">
                            <span><i class="far fa-comment-alt"></i></span>
                            <span><i class="far fa-thumbs-up"></i></span>
                        </div> --}}
                        <div class="col-md-3 pl-4 pr-1">
                            <span>
                                <i class="far fa-comment-alt"></i> <sup>6</sup>
                            </span>
                            <span>
                                <i class="far fa-thumbs-up"></i> <sup>8</sup>
                            </span>
                        </div>
                        <div class="col-md-5 px-0">
                            <span>
                                @if($buyer->user_id==auth()->id())
                                <a class="text-secondary" onclick="buySellBids('{{$buyer->id}}','{{getPostTotalBids($buyer,'buy')}}')"
                                    href="#">
                                    {{getPostTotalBids($buyer)}} bid </a>
                                @else
                                 {{getPostTotalBids($buyer)}} bid 
                                @endif
                            </span>
                            <span>
                                @if($buyer->user_id==auth()->id())
                                <a class="text-secondary" onclick="buySellOrder('{{$buyer->id}}','{{getPostBidOrders($buyer,'buy')}}')"
                                    href="#">
                                    {{getPostBidOrders($buyer)}} orders</a>
                                @else
                                 {{getPostBidOrders($buyer)}} orders
                                @endif
                            </span>
                        </div>
                        <div class="col-md-4">
                            @if((isset($bid) && $bid->status =='pending') || !isset($bid))
                            <input type="text" value="{{$bidAmount}}" class="bidinput position-absolute" data-id="{{$buyer->id}}" size="4"
                                style="align-self: center; width: 60px; right:74px;">
                            <button data-id="{{$buyer->id}}" data-max="{{getCurrentRate($buyer)}}"
                                class="btn btn-primary btn-sm py-0 px-1 position-absolute triggerBid"
                                style="font-size: 0.72rem !important; right:15px;">
                                Place Bid
                            </button>
                            <button data-id="{{$buyer->id}}" data-post-type='buy'
                                class="closebidinput {{$bidAmount?'typing':''}}"
                                style="color: #fff;margin-right: 35px;"><i style="display: none"
                                    class="fas fa-close"></i></button>
                            @endif

                        </div>
                    </div>
                    <div class="card-body p-1">
                        <h6 class="card-title font-weight-bold" style="font-size: 1.125rem !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 1.3em; line-height: 1.2em;">{{ $buyer->buyer_pro_title }}</h6>
                        
                        <p class="card-text"style="font-size: 14px; !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 2.4em; line-height: 1.2em;">
                            {{ $buyer->buyer_pro_description }}
                        </p>

                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6">
                                {{ date_format($buyer->created_at,'d F Y')  }}
                            </div>
                            <div class="col-md-6 text-right">
                                {{  date_diff($buyer->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}

                            </div>
                        </div>
                        <hr class="bg-secondary my-1" style="background-color: #6c757d!important;height:1px;">
                        <div class="text-right mb-3" style="font-size: 0.72rem !important;">
                            {{ $buyer->buyer_location }}
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-3 pr-1">
                                @if (Auth::user()->id == $buyer->user_id)
                                <img src="{{ asset('uploads/avatars/'.App\User::find($buyer->user_id)->avatar) }}" class="rounded-circle w-100 h-auto" alt="person">
                                    
                                @else
                                <img src="{{ asset('uploads/avatars/'.App\User::find($buyer->user_id)->avatar) }}" class="w-100 h-auto" alt="person">
                                    
                                @endif
                            </div>
                            <div class="col-md-9">
                                <h6 class="font-weight-bold">{{ App\User::find($buyer->user_id)->name }}</h6>
                                <div>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6 text-right">
                                {{$buyer->buyer_commission_percentage}}% Referal
                            </div>
                            <div class="col-md-6">
                                <div class="fb-share-button" data-href="http://splashthemepark.com/home" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsplashthemepark.com%2Fhome&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
            <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                    {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                    <img class=""
                        src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                        alt="Card image cap">
                    <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                </a>
                <?php 
            $addCnt++; 
            ?>
            </div>
            <?php } ?>
            @endforeach
            @endisset
            @isset($sellers)
            @foreach ($sellers as $seller)
            <?php
                            $checkPermission = DB::table('user_menu')->where('menu_options_id', '=', '18')->where('user_id', '=', auth()->id())->get()->first();
                            if ($checkPermission) {
                                $permission = 1;
                            } else {
                                $permission = 0;
                            }
                            ?>
            @php
            $bidAmount=bidAmountIfAlreadyBid($seller,'buy');
            $isPostSaved=isSavedPost($seller->id,'buy',auth()->id());
            @endphp
            @php $bid=$seller->bids()->where('user_id',auth()->id())->first() @endphp

            <div class="{{$post_col}}  col-sm-12 hoverSet">

                <div class="card shadow rounded mb-3 set">

                    <div class="position-relative">
                        <a href="#" data-bids="{{getSellPostTotalBids($seller,'sell')}}"
                            data-orders="{{getSellPostBidOrders($seller,'sell')}}" data-isSaved="{{$isPostSaved}}"
                            data-user-name="{{$seller->user->name}}" data-avatar="{{$seller->user->avatar}}"
                            data-all="{{json_encode($seller)}}"
                            onclick="showPostDetails('{{$seller->id}}','{{auth()->id()}}',this)">
                            <img class="card-img-top"
                                src="{{ asset ($seller->seller_featured_image?"uploads/seller/".$seller->seller_featured_image:"/images/image_not_found.jpg")}}"
                                style="height: 200px" alt="Card image cap">
                        </a>
                        <span class="overlay_badge_sell">Sell ${{getSellCurrentRate($seller)}}</span>
                        @if(auth()->id()==$seller->user_id || $permission==1)
                        <span class="overlay_badge_buy buyDelete" id="buyDelete"
                            style="background: none;padding: 0;margin-top: 2px">
                            <a href="#" onclick="deleteBuy('{{$seller->id}}')">
                                <i class="fas fa-trash-alt text-dark bg-white p-1"></i>
                                
                            </a>
                        </span>
                        <span class="overlay_badge_buy buyEdit" id="buyEdit"
                            style="background: none;padding: 0;margin-top: 2px">
                            <a href="#" onclick="buyEdit('{{$seller->id}}','{{$seller->user_id}}')" class="">
                                <i class="fas fa-edit text-primary bg-white p-1"></i>
                            </a>
                                    
                        </span>
                        @endif
                        <span class="overlay_badge_buy savedButton" id="savedButton"
                            style="background: none;padding: 0;margin-top: 2px">
                            <a href="#" onclick="saveBuySell('{{$seller->id}}','{{auth()->id()}}','sell')">
                                
                                    <i id="savedBuy{{$seller->id}}" class="fas fa-heart text-danger bg-white p-1"></i>
                            </a>
                        </span>
                        <div class="text-right mb-2 position-absolute" style="right:0; bottom: -6px;">
                            <span data-time="{{$seller->created_at->addHours($seller->hour)}}"
                                class="bg-danger text-white p-1 countDownTimer" id="showCountDownTimer">
                                {{$seller->hour}}</span>
                        </div>
                    </div>
                    <div class="row mb-2 position-relative" style="font-size: 0.72rem !important;">
                        {{-- <div class="col-md-3 pl-4 pr-1">
                            <span><i class="far fa-comment-alt"></i></span>
                            <span><i class="far fa-thumbs-up"></i></span>
                        </div> --}}
                        <div class="col-md-3 pl-4 pr-1">
                            <span>
                                <i class="far fa-comment-alt"></i> <sup>6</sup>
                            </span>
                            <span>
                                <i class="far fa-thumbs-up"></i> <sup>8</sup>
                            </span>
                        </div>
                        <div class="col-md-5 px-0">
                            <span>
                                @if($seller->user_id==auth()->id())
                                <a class="text-secondary" onclick="sellOrder('{{$seller->id}}','{{getSellPostBidOrders($seller,'sell')}}')"
                                    href="#">
                                    {{getSellPostBidOrders($seller, 'sell')}} orders </a>
                                @else
                                 {{getSellPostBidOrders($seller)}} orders
                                @endif
                            </span>
                            <span>
                                @if($seller->user_id==auth()->id())
                                <a class="text-secondary" onclick="SellBids('{{$seller->id}}','{{getSellPostTotalBids($seller,'sell')}}')"
                                    href="#"> {{getSellPostTotalBids($seller)}} bid </a>
                                @else
                                 {{getSellPostTotalBids($seller)}} bid
                                @endif
                            </span>
                        </div>
                        <div class="col-md-4">
                            @if((isset($bid) && $bid->status =='pending') || !isset($bid))
                            <input type="text" value="{{$bidAmount}}" class="sellinput position-absolute" data-id="{{$seller->id}}"
                                size="4" style="align-self: center; width: 60px; right:74px;">
                            <button data-id="{{$seller->id}}" data-max="{{getCurrentRate($seller)}}"
                                class="triggerSellBid btn btn-primary btn-sm py-0 px-1 position-absolute"
                                style="font-size: 0.72rem !important; right:15px;">
                                Place Bid
                            </button>
                            <button data-id="{{$seller->id}}" data-post-type='buy'
                                class="closebidinput {{$bidAmount?'typing':''}}"
                                style="color: #fff;margin-right: 35px;"><i style="display: none"
                                    class="fas fa-close"></i></button>
                            @endif
                        </div>
                        
                    </div>
                    <div class="card-body p-1">
                        <h6 class="card-title font-weight-bold" style="font-size: 1.125rem !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 1.3em; line-height: 1.2em;">{{ $seller->seller_pro_title }}</h6>
                        <p class="card-text"style="font-size: 14px; !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 2.4em; line-height: 1.2em;">
                            {{ $seller->seller_pro_description }}
                        </p>
          
                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6">
                                {{ date_format($seller->created_at,'d F Y')  }}
                            </div>
                            <div class="col-md-6 text-right">
                                {{  date_diff($seller->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}

                            </div>
                        </div>
                        <hr class="bg-secondary my-1" style="background-color: #6c757d!important; height:1px;">
                        <div class="text-right mb-3" style="font-size: 0.72rem !important;">
                            {{ $seller->seller_location }}
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-3 pr-1">
                                @if (Auth::user()->id == $seller->user_id)
                                <img src="{{ asset('uploads/avatars/'.App\User::find($seller->user_id)->avatar) }}" class="rounded-circle w-100 h-auto" alt="person">
                                    
                                @else
                                <img src="{{ asset('uploads/avatars/'.App\User::find($seller->user_id)->avatar) }}" class="w-100 h-auto" alt="person">
                                    
                                @endif
                            </div>
                            <div class="col-md-9">
                                <h6 class="font-weight-bold">{{ App\User::find($seller->user_id)->name }}</h6>
                                <div>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6 text-right">
                                {{$seller->seller_commission_percentage}}% Referal
                            </div>
                            <div class="col-md-6">
                                <div class="fb-share-button" data-href="http://splashthemepark.com/home" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsplashthemepark.com%2Fhome&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
            <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                    {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                    <img class=""
                        src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                        alt="Card image cap">
                    <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                </a>
                <?php 
            $addCnt++; 
            ?>
            </div>
            <?php } ?>
            @endforeach
            @endisset
            @isset($articles)
            @foreach ($articles as $article)
            <figure class="share-save-icon">
                <img class="share-save-icon-buyr w-4" src="{{asset('img/' . 'referrer.png')}}">
                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" data-value-2="blue"
                    class="share-save-icon-buyr w-4 m-2-45 blue <?php if($article->article_saved_status == 1){ ?>  hide  <?php } ?>"
                    style="" src="{{asset('img/' . 'save2.png')}}">
                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" data-value-2="yellow"
                    class="share-save-icon-buyr w-4 m-2-45 yellow <?php if($article->article_saved_status == 0){ ?>  hide  <?php } ?>"
                    style="" src="{{asset('img/' . 'save3.jpg')}}">
                <?php
                                if($article->article_saved_status != 1 && $article->article_saved_status != 0)
                                {
                                ?>
                <img id="{{$article->id}}" data-value="{{$id}}" data-value1="article" data-value-2="blue"
                    class="share-save-icon-buyr w-4 m-2-45 blue" style="" src="{{asset('img/' . 'save2.png')}}">
                <?php
                                }
                                ?>
                <a href="{{ route('article.show', $article->id) }}">
                    <img src="{{ asset('uploads/article/' . $article->article_featured_image) }}">
                    <figcaption><strong>{{ $article->article_title }}</strong></figcaption>
                    <figcaption class="mt-2"><small
                            class="text-muted">{{ date('M j, Y', strtotime($article->created_at)) }}</small>
                    </figcaption>
                    <figcaption class="float-left"><small class="text-muted"><strong>Article</strong></small>
                    </figcaption>
                </a>
            </figure>
            @endforeach
            @endisset
            @isset($posts)
            @foreach ($posts as $post)
            <?php
                            $countComment = DB::table('comment_post')->where('post_id','=',$post->id)->count();
                            $countLike = DB::table('comment_reactions')->where('post_id','=',$post->id)->where('comment_reaction','=','like')->count();
                            $savedBlogs = App\SavedPost::where('post_type', '=', 'blog')->where('post_id', '=', $post->id)->where('user_id', '=',  Auth::user()->id)->get()->first();
                            ?>
            <div class="{{$post_col}} col-sm-12 hoverSet">

                <div class="card shadow rounded mb-3 set">

                    <div class="position-relative">
                        @if($post->read_amount == 0)
                        <a href="/blod-details/{{$post->id}}" id="viewEventDetails">
                            @if(empty($post->image))
                            <img class="card-img-top" src="/images/image_not_found.jpg" style="height: 200px"
                                alt="Card image cap">
                            @else
                            <img class="card-img-top" src="{{ asset('uploads/blog/' . $post->image) }}"
                                style="height: 200px" alt="Card image cap">
                            @endif
                        </a>
                        @else
                        <a href="javascript:void(0)" id="viewEventDetails"
                            onclick="showMsg('<?php echo $post->heading ?>',<?php echo $post->read_amount  ?>,<?php echo $post->user_id ?>,<?php echo $post->id ?>)">
                            @if(empty($post->image))
                            <img class="card-img-top" src="/images/image_not_found.jpg" style="height: 200px"
                                alt="Card image cap">
                            @else
                            <img class="card-img-top" src="{{ asset('uploads/blog/' . $post->image) }}"
                                style="height: 200px" alt="Card image cap">
                            @endif
                        </a>
                        @endif

                        <span class="overlay_badge_buy px-1" id="" style="background: #F4A460;">Blog
                            @php
                            if ($post->read_amount == 'Free') {
                            echo 'Free';
                            } else {
                            echo '$' . $post->read_amount;
                            }
                            @endphp

                        </span>
                        <span class="overlay_badge_buy eventDelete" id="eventDelete"
                            style="background: none;padding: 0;">

                            @if($id == $post->user_id)
                            <a href="#" onclick="deleteBlog(<?php echo $post->id ?>);">
                                <i class="fas fa-trash-alt text-dark bg-white p-1"></i>
                                
                            </a>
                            @endif
                        </span>
                        <span class="overlay_badge_buy eventEdit" id="eventEdit" style="background: none;padding: 0;">
                            @if(Auth::user()->id == $post->user_id )
                            <a href="#" class=""
                                onclick="blogEdit(<?php echo $post->id ?>,<?php echo $post->user_id ?>)">
                                <i class="fas fa-edit text-primary bg-white p-1"></i>
                            </a>
                            @endif
                        </span>
                        <span class="overlay_badge_buy savedButton" id="savedButton"
                            style="background: none;padding: 0;">
                            <?php
                        if(!empty($savedBlogs))
                        {
                            ?>
                            <a href="#" onclick="savedPost(<?php echo $post->id ?>,<?php echo Auth::user()->id  ?>,'blog')">
                                <i id="savedImage{{$post->id}}" class="fas fa-heart text-danger bg-white p-1"></i>
            
                            </a>
                            <?php
                        }
                        else
                        {
                             ?>
                            <a href="#" onclick="savedPost(<?php echo $post->id ?>,<?php echo Auth::user()->id  ?>,'blog')">
                                <i id="savedImage{{$post->id}}" class="far fa-heart text-danger bg-white p-1"></i>
            
                            </a>
                            <?php
                        }
                        ?>
                        </span>

                    </div>
                    <div class="row mb-2 position-relative" style="font-size: 0.72rem !important;">
                        {{-- <div class="col-md-3 pl-4 pr-1">
                            <span><i class="far fa-comment-alt"></i></span>
                            <span><i class="far fa-thumbs-up"></i></span>
                        </div> --}}
                        <div class="col-md-4 pl-4 pr-1">
                            <span><i class="far fa-comment-alt"></i> <sup>{{$post->total_likes}}</sup> </span>
                            <span><i class="far fa-thumbs-up"></i> <sup>6</sup> </span>
                        </div>
                        <div class="col-md-4 px-1">
                            <span>
                                {{ $post->read_amount }} Readers
                            </span>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary btn-sm py-0 px-1 position-absolute"
                            style="font-size: 0.72rem !important; right:15px;">Read More</button>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <h6 class="card-title font-weight-bold" style="font-size: 1.125rem !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 1.3em; line-height: 1.2em;">{{ $post->heading }}</h6>
                        <div class="card-text mb-2"style="font-size: 14px; !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 2.4em; line-height: 1.2em;">
                            {!! $post->content !!}
                        </div>
                        
                      

                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6">
                                {{ date_format($post->created_at,'d F Y')  }}
                            </div>
                            <div class="col-md-6 text-right">
                                {{  date_diff($post->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}

                            </div>
                        </div>
                        <hr class="bg-secondary my-1" style="background-color: #6c757d!important;height:1px;">

                        <div class="row mb-2">
                            <div class="col-md-3 pr-1">
                                @if (Auth::user()->id == $post->user_id)
                                <img src="{{ asset('uploads/avatars/'.App\User::find($post->user_id)->avatar) }}" class="rounded-circle w-100 h-auto" alt="person">
                                    
                                @else
                                <img src="{{ asset('uploads/avatars/'.App\User::find($post->user_id)->avatar) }}" class="w-100 h-auto" alt="person">
                                    
                                @endif
                            </div>
                            <div class="col-md-9">
                                <h6 class="font-weight-bold">{{ App\User::find($post->user_id)->name }}</h6>
                                <div>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6 text-right">
                                {{$post->referral_per}}% Referal
                            </div>
                            <div class="col-md-6">
                                <div class="fb-share-button" data-href="http://splashthemepark.com/home" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsplashthemepark.com%2Fhome&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
            <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                    {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                    <img class=""
                        src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                        alt="Card image cap">
                    <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                </a>
                <?php 
            $addCnt++; 
            ?>
            </div>
            <?php } ?>
            @endforeach
            @endisset
            @isset($events)
            @foreach($events as $event)
            <?php
                            $userId = Auth::user()->id;
                            $paydate_raw = DB::raw("STR_TO_DATE(`event_date`, '%m/%d/%Y')");
                            $currDate = date('m/d/Y');
                            $getEventDateSavePost = App\EventModal::where('event_date', ">=", $currDate)->where('event_id', $event->id)->orderBy('event_date', 'asc')->get()->first();
                            // echo $getEventDateSavePost;
                            // exit();
                            $savedEvents = App\SavedPost::where('post_type', '=', 'event')->where('post_id', '=', $event->id)->where('user_id', '=', $userId)->get()->first();
                            $eventVisitor = App\EventVisitors::where('user_id', '=', $userId)->where('event_id', $event->id)->get()->first();
                            $checkPermission = DB::table('user_menu')->where('menu_options_id', '=', '18')->where('user_id', '=', $userId)->get()->first();
                            $countShare = DB::table('referral_post')->where('event_id','=',$event->id)->count();

                            //count waiting 
                            $countWaiting = DB::table('event_visitors')->where('event_id','=',$event->id)->where('going_status','pending')->count();
                            //count going
                            $countGoing = DB::table('event_visitors')->where('event_id','=',$event->id)->where('going_status','approved')->count();

                            if ($checkPermission) {
                                $permission = 1;
                            } else {
                                $permission = 0;
                            }
                            ?>

            <?php
                            if($getEventDateSavePost)
                            {
                            ?>

            <div class="{{$post_col}} col-sm-12 hoverSet">

                <div class="card shadow rounded mb-3 set">

                    <div class="position-relative">
                        <?php
                        if(empty($event->event_modal_image))
                        {
                            ?>
                        <a href="#" id="viewEventDetails"
                            onclick="viewEventDetails(<?php echo $event->id ?>,<?php echo $event->user_id ?>)"><img
                                class="card-img-top" src="{{ asset('/images/image_not_found.jpg') }}"
                                style="height: 200px" alt="Card image cap"></a>
                        <?php
                        }
                        else
                        {
                            ?>
                        <a href="#" id="viewEventDetails"
                            onclick="viewEventDetails(<?php echo $event->id ?>,<?php echo $event->user_id ?>)"><img
                                class="card-img-top" src="{{ asset('/uploads/event/'.$event->event_modal_image) }}"
                                style="height: 200px" alt="Card image cap"></a>
                        <?php
                        }
                        ?>


                        <span class="overlay_badge_event">
                            Event <?php
                            if($event->need_approval == 'Yes')
                              {
                                  $needApprove = 1;
                              }
                              else
                              {
                                  $needApprove = 0;
                              }
                              if($event->event_fee_type == 'Not Free')
                              {
                                 echo '$ '.$event->event_fee;
                              }

                              else
                              {
                                echo $event->event_fee_type;
                              }


                              ?>
                        </span>
                        <span class="overlay_badge_buy eventDelete" id="eventDelete"
                            style="background: none;padding: 0;">

                            @if($userId == $getEventDateSavePost->user_id || $permission == 1)
                            <a href="#" onclick="deleteEvent(<?php echo $event->id ?>);">
                                <i class="fas fa-trash-alt text-dark bg-white p-1"></i>
                            </a>
                            @endif
                        </span>
                        <span class="overlay_badge_buy eventEdit" id="eventEdit"
                            style="background: none;padding: 0;">
                            @if(Auth::user()->id == $getEventDateSavePost->user_id || $permission == 1)
                            <a href="#" onclick="eventEdit(<?php echo $event->id ?>,<?php echo $event->user_id ?>)"
                                class="">
                                <i class="fas fa-edit bg-white p-1"></i>
                            </a>
                            @endif
                        </span>
                        <span class="overlay_badge_buy savedButton" id="savedButton"
                            style="background: none;padding: 0;">


                            <?php
                        if(!empty($savedEvents))
                        {
                            ?>
                            <a href="#"
                                onclick="savedPost(<?php echo $event->id ?>,<?php echo Auth::user()->id  ?>, 'event')">
                                
                                <i id="savedImage{{$event->id}}" class="fas fa-heart text-danger bg-white p-1"></i>
                            </a>
                            <?php
                        }
                        else
                        {
                             ?>
                            <a href="#"
                            onclick="savedPost(<?php echo $event->id ?>,<?php echo Auth::user()->id  ?>, 'event')">
                            
                            <i id="savedImage{{$event->id}}" class="far fa-heart text-danger bg-white p-1"></i>
                        </a>
                            <?php
                        }
                        ?>


                        </span>

                    </div>
                    <div class="row mb-2 position-relative" style="font-size: 0.72rem !important;">
                        {{-- <div class="col-md-3 pl-4 pr-1">
                            <span><i class="far fa-comment-alt"></i></span>
                            <span><i class="far fa-thumbs-up"></i></span>
                        </div> --}}
                        <div class="col-md-3 pl-4 pr-1">
                            <span><i class="far fa-comment-alt"></i> <sup>{{$post->total_likes}}</sup> </span>
                            <span><i class="far fa-thumbs-up"></i> <sup>6</sup> </span>
                        </div>
                        <div class="col-md-6 px-1">
                            <span>
                                {{ $event->total_tickets }} Tickets
                            </span>
                            <span>
                                {{ 15 }} Avaliable
                            </span>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="btn btn-outline-info position-absolute px-1 py-0"
                                onclick='eventPay1(<?php echo json_encode($event->event_fee); ?>,<?php echo json_encode($event->user_id); ?>,<?php echo json_encode($event->id); ?>,<?php echo json_encode($getEventDateSavePost->id) ?>,<?php echo json_encode($needApprove); ?>)'
                                style="right:15px; font-size:12px;">Apply</a>
                        </div>

                    </div>
                    <div class="card-body p-1">
                        <h6 class="card-title font-weight-bold" style="font-size: 1.125rem !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 1.3em; line-height: 1.2em;">{{ $event->event_title }}</h6>
                        <div class="card-text mb-2"style="font-size: 14px; !important;
                        font-family: 'Poppins', sans-serif !important; overflow: hidden; max-height: 2.4em; line-height: 1.2em;">
                            {!! $event->event_description !!}
                        </div>

                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6">
                                {{ date_format($event->created_at,'d F Y')  }}
                            </div>
                            <div class="col-md-6 text-right">
                                {{  date_diff($event->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}

                            </div>
                        </div>
                        <hr class="bg-secondary my-1" style="background-color: #6c757d!important;height:1px;">
                        <div class="text-right" style="font-size: 0.72rem !important;">
                            {{ $event->buyer_address }}
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-3 pr-1">
                                @if (Auth::user()->id == $event->user_id)
                                <img src="{{ asset('uploads/avatars/'.App\User::find($event->user_id)->avatar) }}" class="rounded-circle w-100 h-auto" alt="person">
                                    
                                @else
                                <img src="{{ asset('uploads/avatars/'.App\User::find($event->user_id)->avatar) }}" class="w-100 h-auto" alt="person">
                                    
                                @endif
                            </div>
                            <div class="col-md-9">
                                <h6 class="font-weight-bold">{{ App\User::find($event->user_id)->name }}</h6>
                                <div>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="font-size: 0.72rem !important;">
                            <div class="col-md-6 text-right">
                                {{$event->event_referral_commission}}% Referal
                            </div>
                            <div class="col-md-6">

                                    <div class="fb-share-button" data-href="http://splashthemepark.com/home" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsplashthemepark.com%2Fhome&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php  } ?>

            <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
            <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                    {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                    <img class=""
                        src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                        alt="Card image cap">
                    <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                </a>
                <?php 
            $addCnt++; 
            ?>
            </div>
            <?php } ?>
            @endforeach
            <?php while ($addCnt < $allAdsCount) { ?>
            <?php  if($allAdsCount  > 0 && $addCnt < $allAdsCount) { ?>
            <div class="col-sm-12  d-sm-block d-md-none sm-add-root d-lg-none">
                <a href="{{ ($allAds[$addCnt]['image_link']) ? $allAds[$addCnt]['image_link'] : '#' }}"
                    {{ ($allAds[$addCnt]['image_link']) ? 'target="_blank"' : '' }} class="add-link">
                    <img class=""
                        src="{{$allAds[$addCnt]['image']?"uploads/adsimages/".$allAds[$addCnt]['image']:"/images/image_not_found.jpg"}}"
                        alt="Card image cap">
                    <span class="">{{$allAds[$addCnt]['adds_name']}}</span>
                </a>
                <?php 
            $addCnt++; 
            ?>
            </div>
            <?php } } ?>
            @endisset
            @isset($result)
            @foreach($result as $res)
            <div class="col-md-3    ">
                <div class="card text-center card-width">
                    <div class="card-header filter">
                        <div class="row no-margin" style="width: 100%">
                            <div class="col-md-2" style="padding-right: 0; padding-left: 2px">
                                <div class="img-container">
                                    <img width="100%" src="{{ url('/uploads/avatars').'/' . $res['user_pic'] }}">
                                </div>
                            </div>
                            <div class="col-md-5" style="padding-left: 0;text-align: left">
                                <div class="title-container ml-2"><span
                                        style="position: absolute;bottom: 0;">{{ $res['user_name'] }}</span>
                                </div>
                            </div>
                            <div class="col-md-5 align-self-end">
                                <input id="ownRatingMobileCard" name="ownRating"
                                    class="rating rating-loading own-rating rating-xs ownRatingMobileCard"
                                    value="{{averageReview($res['user_id'])}}" style="padding-top: 8px;">

                            </div>
                        </div>

                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <img width="100%" src="{{url('/uploads') .$res['image']}}">
                            </div>
                        </div>
                        <h5 class="card-title">{{$res['heading']}}</h5>
                    </div>
                    <div class="card-footer text-muted">
                        {!! $res['btn'] !!}
                    </div>
                </div>
            </div>
            @endforeach
            @endisset
        </div>
    </div>
    <style type="text/css">
        /*.hoverSet :hover {
                 border: 5px solid ;
                 overflow: hidden;
             }*/
        .hoverSet:hover .set {

            border: 4px solid orange;
            padding: 2px;

        }

        #savedButton {
            display: none;
        }

        #eventDelete {
            display: none;
        }

        #buyDelete {
            display: none;
        }

        #eventEdit {
            display: none;
        }

        #buyEdit {
            display: none;
        }

        .hoverSet:hover #savedButton {
            display: block;
        }

        .hoverSet:hover #eventDelete {
            display: block;
        }

        .hoverSet:hover #buyDelete {
            display: block;
        }

        .hoverSet:hover #eventEdit {
            display: block;
        }

        .hoverSet:hover #buyEdit {
            display: block;
        }

        .overlay_badge_buy:hover .editButton {
            border: 1px solid orange;
            padding: 2px;
        }

        .overlay_badge_buy:hover .deleteButton {
            border: 1px solid orange;
            padding: 2px;
        }

        .overlay_badge_buy:hover .savedButton {
            border: 1px solid orange;
            padding: 2px;
        }
    </style>

    <div class="row">
        {{--<div class="col-md-3">--}}
        {{--<div class="card text-center card-width">--}}
        {{--<div class="card-header">--}}
        {{--<div class="row no-margin" style="width: 100%">--}}
        {{--<div class="col-md-2" style="padding-right: 0; padding-left: 2px">--}}
        {{--<div class="img-container">--}}
        {{--<img width="100%" src="http://hi5working.test/uploads/avatars/1542355689.jpg">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-5" style="padding-left: 0;text-align: left">--}}
        {{--<div class="title-container ml-2"><span style="position: absolute;bottom: 0;">Elu</span></div>--}}
        {{--</div>--}}
        {{--<div class="col-md-5 align-self-end">--}}
        {{--<input id="ownRatingMobileCard" name="ownRating" class="rating rating-loading own-rating rating-xs ownRatingMobileCard"--}}
        {{--value="{{averageReview(Auth::user()->id)}}" style="padding-top: 8px;">--}}

        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}
        {{--<div class="card-body">--}}
        {{--<div class="row">--}}
        {{--<div class="col">--}}
        {{--<img width="100%" src="http://hi5working.test/uploads/avatars/1542355689.jpg">--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<h5 class="card-title">Elu@Elu.com</h5>--}}
        {{--</div>--}}
        {{--<div class="card-footer text-muted">--}}
        {{--<a href="#" class="btn btn-primary">Go somewhere</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}


    </div>
</div>
@endsection
@section('extra-JS')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".share-save-icon-buyr").click(function () {
            var user_id = $(this).attr("data-value");
            var post_id = $(this).attr("id");
            var post_type = $(this).attr("data-value1");
            var saved_cls = $(this).attr("data-value-2");
            var status = "";
            if (saved_cls == "blue") {
                $(this).addClass('hide');
                $(this).next().removeClass('hide');
                // $(".blue").addClass('hide');
                // $(".yellow").removeClass('hide');
                status = 1;
            } else {
                $(this).addClass('hide');
                $(this).prev().removeClass('hide');
                // $(".yellow").addClass('hide');
                // $(".blue").removeClass('hide');
                status = 0;
            }
            $.ajax({
                url: "SavePost",
                type: "POST",
                data: {
                    user_id: user_id,
                    post_id: post_id,
                    post_type: post_type,
                    status: status
                },
                dataType: "JSON",
                success: function (data) {
                    console.log(data);

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert('Something went wrong');
                }
            });
        });
    });

    function showMsg(heading, amount, owner_id, id) {
        Swal.fire({
            title: 'Pay To Read?',
            text: "You have to pay $" + amount + " to read!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'green',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Pay'
        }).then((result) => {
            if (result.value) {
                $.ajax({

                    url: '/blog/pay-to-read/' + amount + '/' + owner_id + '/' + id,
                    type: 'GET',

                    success: function (response) {

                        console.log(response);
                        Swal.fire(
                            'Payment Done!',
                            'You can now read blog.',
                            'success'
                        ).then((result) => {
                            if (result.value) {
                                window.location = '/blod-details/' + id;
                            }
                        });

                    }
                });
            }
        });
    }
</script>
<div id="fb-root"></div>
<script>
    //shere event

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=403257377055066";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function fb_share(dynamic_link, dynamic_title) {
        var app_id = '403257377055066';
        var pageURL = "https://www.facebook.com/dialog/feed?app_id=" + app_id + "&link=" + dynamic_link;
        var w = 600;
        var h = 400;
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        window.open(pageURL, dynamic_title,
            'toolbar=no, location=no, directories=no,status=no, menubar=yes, scrollbars=no, resizable=no, copyhistory=no, width=' +
            800 + ',height=' + 650 + ', top=' + top + ', left=' + left)

        return false;
    }
</script>
<script>
    document.getElementById('shareBtn').onclick = function() {
      FB.ui({
        display: 'popup',
        method: 'share',
        href: 'https://developers.facebook.com/docs/',
      }, function(response){});
    }
    </script>
      <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code -->
  <div class="fb-share-button" 
    data-href="https://www.yousoftme.com" 
    data-layout="button">
  </div>
@endsection