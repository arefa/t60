@extends('layouts.app')
@section('content')

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-md-3">
            @foreach ($buyers as $buyer)
            <div class="card shadow rounded mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <span class="bg-success text-white p-1">Buy ${{$buyer->rate}}</span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="p-1"><i class="far fa-heart"></i></span>
                    </div>
                </div>
                <img class="card-img-top" src="{{ asset('uploads/buyer/'.$buyer->buyer_featured_image) }}"
                    class="w-100 h-auto" alt="photo">
                <div class="text-right mb-2">
                        <span data-time="{{$buyer->created_at->addHours($buyer->hour)}}" class="bg-danger text-white p-1 countDownTimer"
                            id="showCountDownTimer">
                            {{$buyer->hour}}</span>
                </div>
                <div class="row mb-2" style="font-size: 0.72rem !important;">
                    <div class="col-md-3 pl-4 pr-1">
                        <span><i class="far fa-comment-alt"></i></span>
                        <span><i class="far fa-thumbs-up"></i></span>
                    </div>
                    <div class="col-md-9 px-1">
                        <span>5 bid</span>
                        <span>2 order</span>
                        <button type="submit" class="btn btn-primary btn-sm py-0 px-1"
                            style="font-size: 0.72rem !important;">Place Bid</button>
                    </div>
                </div>
                <div class="card-body p-1">
                    <h6 class="card-title font-weight-bold">{{ $buyer->buyer_pro_title }}</h6>
                    <p class="card-text">{{ $buyer->buyer_pro_description }}</p>
                    <div class="row mb-3" style="font-size: 0.72rem !important;">
                        <div class="col-md-6">
                            {{ date_format($buyer->created_at,'Y-m-d')  }}
                        </div>
                        <div class="col-md-6 text-right">
                            {{  date_diff($buyer->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago')  }}

                        </div>
                    </div>
                    <hr class="text-dark">
                    <div class="text-right" style="font-size: 0.72rem !important;">
                        {{ $buyer->buyer_location }}
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-3 pr-1">
                            <img src="{{ asset('uploads/avatars/person.png') }}" class="w-100 h-auto" alt="person">
                        </div>
                        <div class="col-md-9">
                            <h6 class="font-weight-bold">{{ App\User::find($buyer->user_id)->name }}</h6>
                            <div>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-size: 0.72rem !important;">
                        <div class="col-md-6 text-right px-1">
                            {{$buyer->buyer_commission_percentage}}% Referal
                        </div>
                        <div class="col-md-6 px-1">
                            <button id="shareBtn" type="button" class="btn btn-primary btn-sm py-0 px-1"
                                style="font-size: 0.72rem !important;"><span><i class="fas fa-share"></i></span>
                                Share</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="col-md-3">
            @foreach ($sellers as $seller)
            <div class="card shadow rounded mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <span class="bg-danger text-white p-1">Sell ${{$seller->rate}}</span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="p-1"><i class="far fa-heart"></i></span>
                    </div>
                </div>
                <img class="card-img-top" src="{{ asset('uploads/seller/'.$seller->seller_featured_image) }}"
                    class="w-100 h-auto" alt="photo">
                <div class="text-right mb-2">
                    <span data-time="{{$seller->created_at->addHours($seller->hour)}}" class="bg-danger text-white p-1 countDownTimer"
                        id="showCountDownTimer">
                        {{$seller->hour}}</span>
                </div>
                <div class="row mb-2" style="font-size: 0.72rem !important;">
                    <div class="col-md-3 pl-4 pr-1">
                        <span><i class="far fa-comment-alt"></i></span>
                        <span><i class="far fa-thumbs-up"></i></span>
                    </div>
                    <div class="col-md-9 px-1">
                        <span>5 bid</span>
                        <span>2 order</span>
                        <button type="submit" class="btn btn-primary btn-sm py-0 px-1"
                            style="font-size: 0.72rem !important;">Place Bid</button>
                    </div>
                </div>
                <div class="card-body p-1">
                    <h6 class="card-title font-weight-bold">{{$seller->seller_pro_title}}</h6>
                    <p class="card-text">{{$seller->seller_pro_description}}</p>
                    <div class="row mb-3" style="font-size: 0.72rem !important;">
                        <div class="col-md-6">
                            {{ date_format($seller->created_at,'Y-m-d')  }}
                        </div>
                        <div class="col-md-6 text-right">
                            {{ date_diff($seller->created_at,date_create(date("Y-m-d h:i:s")))->format('%d Days ago') }}
                        </div>
                    </div>
                    <hr class="text-dark">
                    <div class="text-right" style="font-size: 0.72rem !important;">
                        {{$seller->seller_location}}
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-3 pr-1">
                            <img src="{{ asset('uploads/avatars/person.png') }}" class="w-100 h-auto" alt="person">
                        </div>
                        <div class="col-md-9">
                            <h6 class="font-weight-bold">{{ App\User::find($seller->user_id)->name }}</h6>
                            <div>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-size: 0.72rem !important;">
                        <div class="col-md-6 text-right px-1">
                            {{$seller->seller_commission_percentage}}% Referal
                        </div>
                        <div class="col-md-6 px-1">
                            <button id="shareBtn" type="button" class="btn btn-primary btn-sm py-0 px-1"
                                style="font-size: 0.72rem !important;"><span><i class="fas fa-share"></i></span>
                                Share</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="col-md-3">
            @foreach ($posts as $post)
            <div class="card shadow rounded mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <span class="text-white p-1" style="background-color:orangered;">Blog $789</span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="p-1"><i class="far fa-heart"></i></span>
                    </div>
                </div>
                <img class="card-img-top" src="{{ asset('uploads/blog/'.$post->image) }}" class="w-100 h-auto"
                    alt="photo">
                <div class="text-right mb-2">
                    {{-- <span data-time="{{$post->created_at->addHours($post->hour)}}" class="bg-danger text-white p-1 countDownTimer"
                        id="showCountDownTimer">
                        {{$post->hour}}</span> --}}
                </div>
                <div class="row mb-2" style="font-size: 0.72rem !important;">
                    <div class="col-md-3 pl-4 pr-1">
                        <span><i class="far fa-comment-alt"></i></span>
                        <span><i class="far fa-thumbs-up"></i></span>
                    </div>
                    <div class="col-md-9 px-1">
                        <span>5 bid</span>
                        <span>2 order</span>
                        <button type="submit" class="btn btn-primary btn-sm py-0 px-1"
                            style="font-size: 0.72rem !important;">Place Bid</button>
                    </div>
                </div>
                <div class="card-body p-1">
                    <h6 class="card-title font-weight-bold">{{$post->heading }}</h6>
                    <div class="card-text">{!! $post->content !!}</div>
                    <div class="row mb-3" style="font-size: 0.72rem !important;">
                        <div class="col-md-6">
                            18 Nov 2020
                        </div>
                        <div class="col-md-6 text-right">
                            3 months ago
                        </div>
                    </div>
                    <hr class="text-dark">
                    <div class="text-right" style="font-size: 0.72rem !important;">
                        CA, USA
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-3 pr-1">
                            <img src="{{ asset('uploads/avatars/person.png') }}" class="w-100 h-auto" alt="person">
                        </div>
                        <div class="col-md-9">
                            <h6 class="font-weight-bold">{{ App\User::find($post->user_id)->name }}</h6>
                            <div>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-size: 0.72rem !important;">
                        <div class="col-md-6 text-right px-1">
                            {{$post->referral_per }}% Referal
                        </div>
                        <div class="col-md-6 px-1">
                            <button id="shareBtn" type="button" class="btn btn-primary btn-sm py-0 px-1"
                                style="font-size: 0.72rem !important;"><span><i class="fas fa-share"></i></span>
                                Share</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="col-md-3">
            @foreach ($events as $event)
            <div class="card shadow rounded mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <span class="text-white p-1" style="background-color:purple;">Event ${{ $event->event_fee }}
                        </span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="p-1"><i class="far fa-heart"></i></span>
                    </div>
                </div>
                <img class="card-img-top" src="{{ asset('uploads/event/'.$event->event_modal_image) }}"
                    class="w-100 h-auto" alt="photo">
                <div class="text-right mb-2">
                    {{-- <span data-time="{{$event->created_at->addHours($event->hour)}}" class="bg-danger text-white p-1 countDownTimer"
                        id="showCountDownTimer">
                        {{$event->hour}}</span> --}}
                </div>
                <div class="row mb-2" style="font-size: 0.72rem !important;">
                    <div class="col-md-3 pl-4 pr-1">
                        <span><i class="far fa-comment-alt"></i></span>
                        <span><i class="far fa-thumbs-up"></i></span>
                    </div>
                    <div class="col-md-9 px-1">
                        <span>5 bid</span>
                        <span>2 order</span>
                        <button type="submit" class="btn btn-primary btn-sm py-0 px-1"
                            style="font-size: 0.72rem !important;">Place Bid</button>
                    </div>
                </div>
                <div class="card-body p-1">
                    <h6 class="card-title font-weight-bold">{{ $event->event_title }}</h6>
                    <p class="card-text">{!! $event->event_description !!}</p>
                    <div class="row mb-3" style="font-size: 0.72rem !important;">
                        <div class="col-md-6">
                            18 Nov 2020
                        </div>
                        <div class="col-md-6 text-right">
                            3 months ago
                        </div>
                    </div>
                    <hr class="text-dark">
                    <div class="text-right" style="font-size: 0.72rem !important;">
                        {{ $event->event_address }}
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-3 pr-1">
                            <img src="{{ asset('uploads/avatars/person.png') }}" class="w-100 h-auto" alt="person">
                        </div>
                        <div class="col-md-9">
                            <h6 class="font-weight-bold">{{ App\User::find($event->user_id)->name }}</h6>
                            <div>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="font-size: 0.72rem !important;">
                        <div class="col-md-6 text-right px-1">
                            {{ $event->event_referral_commission }}% Referal
                        </div>
                        <div class="col-md-6 px-1">
                            <button id="shareBtn" type="button" class="btn btn-primary btn-sm py-0 px-1"
                                style="font-size: 0.72rem !important;"><span><i class="fas fa-share"></i></span>
                                Share</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection
@push('footerJs')
<script>
    // facebook Share
    document.getElementById('shareBtn').onclick = function () {
        FB.ui({
            display: 'popup',
            method: 'share',
            href: 'https://developers.facebook.com/docs/',
        }, function (response) {});
    }
</script>
@endpush